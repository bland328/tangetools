CMD = blink 2grep 2search burncpu duplicate-packets em encdir field	\
	forever fxkill G gitnext gitundo goodpasswd histogram mtrr	\
	mirrorpdf neno off parsort pdfman pidcmd pidtree plotpipe	\
	puniq ramusage rand rclean rina rn rrm seekmaniac shython	\
	sound-reload splitvideo stdout swapout T timestamp tracefile	\
	transpose upsidedown vid w4it-for-port-open whitehash		\
	wifi-reload wssh ytv yyyymmdd

all: blink/blink.1 2search/2grep.1 2search/2search.1			\
	burncpu/burncpu.1 encdir/encdir.1 field/field.1 G/G.1		\
	gitnext/gitnext.1 gitundo/gitundo.1 goodpasswd/goodpasswd.1	\
	histogram/histogram.1 mirrorpdf/mirrorpdf.1 neno/neno.1		\
	off/off.1 parsort/parsort.1 pdfman/pdfman.1 pidcmd/pidcmd.1	\
	pidtree/pidtree.1 plotpipe/plotpipe.1 puniq/puniq.1		\
	rand/rand.1 rina/rina.1 rn/rn.1 rrm/rrm.1			\
	seekmaniac/seekmaniac.1 shython/shython.1			\
	sound-reload/sound-reload.1 splitvideo/splitvideo.1		\
	stdout/stdout.1 timestamp/timestamp.1 tracefile/tracefile.1	\
	transpose/transpose.1 T/T.1 upsidedown/upsidedown.1 vid/vid.1	\
	wifi-reload/wifi-reload.1 wssh/wssh.1 ytv/ytv.1			\
	yyyymmdd/yyyymmdd.1

%.1: %
	pod2man $< > $@

install:
	mkdir -p /usr/local/bin
	parallel eval ln -sf `pwd`/*/{} /usr/local/bin/{} ::: $(CMD)
	mkdir -p /usr/local/share/man/man1
	parallel ln -sf `pwd`/{} /usr/local/share/man/man1/{/} ::: */*.1
	mkdir -p $(HOME)/.local/share/vlc/lua/extensions
	parallel ln -sf `pwd`/{} $(HOME)/.local/{=s:[^/]+/[^/]+/::=} ::: */dotlocal/*/*/*/*/*.lua
